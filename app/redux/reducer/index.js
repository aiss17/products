import { combineReducers } from "redux";
import { ProductReducer } from "./ProductReducer";

const reducer = combineReducers({
    ProductReducer,
});

export default reducer;
