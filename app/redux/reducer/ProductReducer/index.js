const initialStateHome = {
    list: []
};

// reducer
export const ProductReducer = (state = initialStateHome, action) => {
    switch (action.type) {
        case "SET_LIST":
            console.log("update ketika login dan ", action.inputValue);
            return {
                ...state,
                list: action.inputValue 
            };

        default:
            return state;
    }
};
