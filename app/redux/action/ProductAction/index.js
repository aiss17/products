const SET_LIST = (value) => {
    return { type: "SET_LIST", inputValue: value };
};

export { SET_LIST };
