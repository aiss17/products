import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { ListProduct } from "../pages";

const Stack = createNativeStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="List Product">
      <Stack.Screen
        name="List Product"
        component={ListProduct}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default Router;
